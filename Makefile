all:
	@nasm -felf64 hang.asm
	@ld hang.o -o hang

clean:
	@(rm *.o hang || echo "Already cleaned!") && echo "Cleaned!"
